;;;; multicp.lisp

;; (ql:quickload :multicp)
(in-package :multicp)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Description:  Evaluate a YAML configuration file to copy files from the
;;               local system to a test system using SCP and to execute
;;               commands on a test system using SSH.
;;
;;   ----- Python Versions -----------------------------------------------
;;
;;   v1.0        21-Jul-2006  First version in Python
;;   v1.1        19-Apr-2013  Allow local copies without SSH
;;   v1.2        23-Apr-2013  Use specific port for SCP
;;   v1.2.1      24-Apr-2013  Enable SSH automatically
;;   v2.0        31-Jul-2013  Remove dependency to psCfgData2
;;   v2.1        14-Aug-2013  Allow execution of commands
;;   v2.2        28-Oct-2013  List configurations for selection
;;   v2.2.1      08-Jan-2014  Allow multi-line file lists
;;   v2.3        07-May-2014  Check existence of source files
;;   v2.3.1      08-May-2014  Make it more Windows ready (still incomplete)
;;   v3.0        04-Dec-2014  Refactored, Allow different target file name,
;;                            Improved error handling
;;   v3.1        18-Dec-2014  Allow specification of explicit config file,
;;                            Should be usable on Windows now (not tested)
;;   v3.2        23-Nov-2016  Fixed problems with the use for local copies
;;   v3.3        22-Dec-2017  Allow specification of system credentials
;;                            on commandline
;;   ----- Common Lisp Versions ------------------------------------------
;;
;;   4.0cl       20-Jan-2021  Rewrite in Common Lisp
;;   4.1         17-Feb-2021  Added function to list configurations
;;   4.1.1       24-Feb-2021  Fix 'unmatching number' problem with '$' in commands
;;   4.2         10-Mar-2021  Allow template usage in file names
;;   4.3         15-Mar-2021  Added dry-run option
;;   4.4         25-Jun-2021  Refactored to reduce complexity
;;   4.4.1       26-Oct-2022  Changed parameters, ask for correct host
;;   4.4.2       07-Jun-2023  Some refactoring
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defparameter *tool-name* "multicp")
(defparameter *tool-copyright* "Copyright (C) 2006 - 2023")
(defparameter *tool-author* "Peter Stoldt")
(defparameter *tool-version* "4.4.2")
(defparameter *tool-last-modified* "07-Jun-2023")
(defparameter *version* (format nil "~a ~a [~a] -- ~a by ~a"
                                *tool-name* *tool-version* *tool-last-modified*
                                *tool-copyright* *tool-author*))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defparameter *config-dir* (concatenate 'string (uiop:getenv "HOME") "/.config/multicp/")
  "Default directory for configuration files.")
(defparameter *system-section* "system"
  "Section for the specification of the target system.")
(defparameter *settings-section* "settings"
  "Section for user defined settings to be used in the configuration.")
(defparameter *init-section* "init"
  "Section for always executed init tasks.")
(defparameter *cleanup-section* "cleanup"
  "Section for always executed cleanup tasks.")

(defparameter *non-file-sections* (list *system-section* *settings-section*)
  "List of sections in the configuration that do not deal with files.")
(defparameter *pre-post-sections* (list *init-section* *cleanup-section*)
  "List of sections used for init and cleanup.")

(defvar *cmd-opts* nil
  "Commandline options of the tool call.")
(defvar *cmd-free-args* nil
  "Commandline free arguments")
(defvar *config* nil
  "Hashmap containing the specifications from the parsed YAML configuration file.")
(defvar *target-system* nil
  "Hashmap containing parameter and credentials of the target system.")
(defvar *settings* nil
  "Hashmap containing replacement settings for templates.")
(defvar *do-execute* t
  "If do-execute is nil, only print the commands without executing it.")

;; Templates
;;;;;;;;;;;;;;

(defun apply-settings (template)
  "Apply settings to the given template string. This is done recursively."
  (labels ((rec-apply (str)
             (when str
               (let ((s1 (cl-ppcre:scan-to-strings "\\$\\{[^\\}]+\\}" str)))
                 (if s1
                     (let* ((setting (subseq s1 2 (1- (length s1))))
                            (replacement (gethash setting *settings*)))
                       (if replacement
                           (progn
                             ;;(format t "Found: ~s -> ~s -> ~s~%" s1 setting replacement)
                             (rec-apply (cl-ppcre:regex-replace-all (concatenate 'string "\\$\\{" setting "\\}") str replacement)))
                           (progn
                             (format t "Error: Setting '~a' in template '~a' could not be found.~%" setting template)
                             nil)))
                     str)))))
    (rec-apply template)))

(defun templates-ok-p ()
  "Check if the template has a valid syntax and check if all required user settings are defined."
  (let ((templates-ok t))
    (loop :for key :being :the hash-keys :of *config*
            :using (hash-value section-conf) :do
              (unless (member key *non-file-sections* :test #'string=)
                (loop :for sect :in '("fromDir" "toDir") :do
                  (let ((template (gethash sect section-conf)))
                    (when template
                      (unless (apply-settings template)
                        (setf templates-ok nil)))))
                (let ((files (gethash "files" section-conf)))
                  (when files
                    (loop :for file :in files :do
                      (unless (apply-settings file)
                        (setf templates-ok nil)))))
                (let ((cmds (gethash "commands" section-conf)))
                  (when cmds
                    (loop :for cmd :in cmds :do
                      (unless (apply-settings cmd)
                        (setf templates-ok nil)))))))
    templates-ok))

;; Files
;;;;;;;;;;

(defun src-and-target (from-dir to-dir raw-file)
  "Get a cons containing the source file and the target file location."
  ;;(format t "from-dir: '~a'~%to-dir: '~a'~%" from-dir to-dir)
  (let ((file-strings (uiop:split-string raw-file :separator ">>")))
    (cons (concatenate 'string from-dir "/" (string-trim " " (car file-strings)))
          (concatenate 'string to-dir "/" (string-trim " " (car (last file-strings)))))))

(defun files-to-copy (section-conf)
  "Return an alist of the files to copy in the format ((\"<from>\" . \"<to>\") ...)."
  (let ((from-dir (apply-settings (gethash "fromDir" section-conf)))
        (to-dir (apply-settings (gethash "toDir" section-conf)))
        (raw-files (gethash "files" section-conf)))
    (loop :for file :in raw-files
          :collect (src-and-target from-dir to-dir (apply-settings file)))))

(defun source-files-exist-p ()
  "Check if all defined source files of the file sections are available."
  (let ((files-ok t))
    (loop :for key :being :the hash-keys :of *config*
            :using (hash-value section-conf) :do
              (unless (member key *non-file-sections* :test #'string=)
                (let ((files (files-to-copy section-conf)))
                  (loop :for file :in files :do
                    (unless (probe-file (first file))
                      (format t "Error: File '~a' in section '~a' not found.~%" (first file) key)
                      (setf files-ok nil))))))
    files-ok))

;; Execution
;;;;;;;;;;;;;;

(defun copy-file (file)
  "Use the external 'scp' command to copy a file."
  (let ((host (gethash "targetSystem" *target-system*))
        (port (gethash "targetPort" *target-system*))
        (user (gethash "targetUser" *target-system*)))
    (format t "  copy: '~a' -> '~a'~%" (car file) (cdr file))
    (when *do-execute*
      (uiop:run-program (list "scp" "-P" (write-to-string port) (car file) (format nil "~a@~a:~a" user host (cdr file)))
                        :output :interactive :input :interactive :error-output t))))

(defun copy-files (section-conf)
  "Copy files of a section to the target system."
  (let ((files (files-to-copy section-conf)))
    (loop :for file :in files :do
      (copy-file file))))

(defun execute-cmd (cmd)
  "Use the external 'ssh' command to execute a task on the target system."
  (let ((host (gethash "targetSystem" *target-system*))
        (port (gethash "targetPort" *target-system*))
        (user (gethash "targetUser" *target-system*)))
    (format t "  exec: '~a'~%" cmd)
    (when *do-execute*
      (uiop:run-program (list "ssh" "-p" (write-to-string port) (format nil "~a@~a" user host) cmd)
                        :output :interactive :input :interactive :error-output t))))

(defun execute-commands (section-conf)
  "Execute defined commands of a section."
  (loop :for cmd-tmpl :in (gethash "commands" section-conf) :do
    (execute-cmd (apply-settings cmd-tmpl))))

(defun execute-section (section)
  "Execute file copy tasks and commands of a section."
  (format t "~%=== ~A ===~%~%" section)
  (let ((section-conf (gethash section *config*)))
    (when section-conf
      (copy-files section-conf)
      (execute-commands section-conf))))

;; Options stuff
;;;;;;;;;;;;;;;;;;

(defun unknown-option (condition)
  (format t "Warning: ~s option is unknown!~%" (opts:option condition))
  (invoke-restart 'opts:skip-option))

(defmacro when-option ((options opt) &body body)
  `(let ((it (getf ,options ,opt)))
     (when it
       ,@body)))

(defun usage ()
  "Print tool usage"
  (opts:describe
   :prefix "Multicp - a tool to copy files to a test system and execute commands on it."
   :usage-of "multicp"
   :args "config-file")
  (format t "Version: ~a~%" *version*))

(defun list-configurations ()
  "List all available configurations"
  (let ((files (uiop:directory-files *config-dir*)))
    (format t "Available configurations:~%~%")
    (dolist (file files)
      (let* ((file-str (car (last (uiop:split-string (namestring file) :separator "/"))))
             (len (length file-str)))
        (format t "  - ~a~%" (subseq file-str 0 (- len 5))))))
  (terpri))

(defun parse-cmd-opts ()
  (opts:define-opts
    (:name :help :description "print this help text" :short #\h :long "help")
    (:name :port :description "port for the ssh connection" :short #\p :long "port" :arg-parser #'parse-integer)
    (:name :host :description "target host name" :short #\H :long "host" :arg-parser #'identity)
    (:name :user :description "target system user" :short #\u :long "user" :arg-parser #'identity)
    (:name :list :description "list all configurations" :short #\l :long "list")
    (:name :dryrun :description "print only, don't execute" :short #\d :long "dryrun")
    (:name :version :description "show version information" :short #\v :long "version"))

  ;; check parameters
  (multiple-value-bind (options free-args)
      (handler-case
          (handler-bind ((opts:unknown-option #'unknown-option))
            (opts:get-opts))
        (opts:missing-arg (condition)
          (format t "Fatal: option ~s needs an argument!~%"
                  (opts:option condition)))
        (opts:arg-parser-failed (condition)
          (format t "Fatal: cannot parse ~s as argument of ~s~%"
                  (opts:raw-arg condition)
                  (opts:option condition)))
        (opts:missing-required-option (con)
          (format t "Fatal: ~a~%" con)
          (throw :leave-now t)))

    (when-option (options :help)
      (usage)
      (throw :leave-now t))

    (when-option (options :list)
      (list-configurations)
      (throw :leave-now t))

    (when-option (options :version)
      (format t "Version: ~a~%" *version*)
      (throw :leave-now t))

    (when-option (options :dryrun)
      (format t "Info: 'dryrun' set - Just printing commands without execution~%")
      (setf *do-execute* nil))

    (unless (= (list-length free-args) 1)
      (format t "Error: Please specify a configuration: (~{~a~^, ~})~%" free-args)
      (usage)
      (throw :leave-now t))

    (setf *cmd-opts* options)
    (setf *cmd-free-args* free-args)))

;; Main
;;;;;;;;;

(defun main ()
  (catch :leave-now
    (parse-cmd-opts)
    (let ((conf-file (pathname (concatenate 'string *config-dir* (first *cmd-free-args*) ".yaml"))))

      (setf *config* (handler-case (yaml:parse conf-file)
                       (sb-ext:file-does-not-exist (c)
                         (format t "Error:~%~a~%" c)
                         (return-from main))))
      (setf *settings* (gethash *settings-section* *config*))
      (setf *target-system* (gethash *system-section* *config*))

      ;; overwrite system settings from the config file with the specified ones
      (when-option (*cmd-opts* :host)
        (setf (gethash "targetSystem" *target-system*) it))
      (when-option (*cmd-opts* :port)
        (setf (gethash "targetPort" *target-system*) it))
      (when-option (*cmd-opts* :user)
        (setf (gethash "targetUser" *target-system*) it))

      (format t "~%=== Target System ===~%~%")
      (format t "  User:   ~a~%" (gethash "targetUser" *target-system*))
      (format t "  System: ~a~%" (gethash "targetSystem" *target-system*))
      (format t "  Port:   ~a~%" (gethash "targetPort" *target-system*))
      (format t "~%--> Is this correct ")
      (finish-output)
      (unless (y-or-n-p)
        (format t "OK, aborting ...~%")
        (throw :leave-now t))

      ;; Check if all settings can be placed correctly into the given templates
      (unless (templates-ok-p)
        (format t "Cannot place all user settings in the template.~%")
        (throw :leave-now t))

      ;; Check if all defined source files exist
      (unless (source-files-exist-p)
        (format t "Not all configured source files could be found. Please check the configuration.~%")
        (throw :leave-now t))

      ;; Execute 'init' section
      (handler-case (execute-section *init-section*)
        (uiop:subprocess-error (c)
          (format t "Error:~%~a~%" c)
          (throw :leave-now t)))

      ;; Execute all file sections
      (handler-case
          (loop :for key :being :the hash-keys :of *config* :do
            (unless (or (member key *non-file-sections* :test #'string=)
                        (member key *pre-post-sections* :test #'string=))
              (execute-section key)))
        (uiop:subprocess-error (c)
          (format t "Error:~%~a~%" c)
          (throw :leave-now t)))

      ;; Execute 'cleanup' section
      (handler-case (execute-section *cleanup-section*)
        (uiop:subprocess-error (c)
          (format t "Error:~%~a~%" c)
          (throw :leave-now t))))))
