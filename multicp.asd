;;;; multicp.asd

(asdf:defsystem #:multicp
  :description "Multicp is a tool to copy files to a test system and execute commands on it."
  :author "Peter Stoldt - peter-stoldt@web.de"
  :license  "GPL 3"
  :version "4.4"
  :serial t
  :depends-on ("cl-yaml"
               "cl-ppcre"
               "unix-opts")
  :components ((:file "package")
               (:file "multicp")))
