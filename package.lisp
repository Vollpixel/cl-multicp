;;;; package.lisp

(uiop:define-package #:multicp
  (:shadowing-import-from :unix-opts :describe)
  (:use #:cl)
  (:use #:cl-yaml)
  (:use #:cl-ppcre)
  (:use #:unix-opts))
