#+TITLE:  multicp
#+AUTHOR: Peter Stoldt
#+EMAIL:  peter-stoldt@web.de
#+DATE:   November 18, 2020 -- \today

------------------------------------------------------------

* multicp

  Multicp is a tool to copy a configured set of files to a target system - e.g. a test
  system - and to execute commands on the system. Files and commands are configured in a
  configuration file in YAML format. To copy the files and to execute the commands, SCP and
  SSH are used.

  This project is implemented in Common Lisp and tested with [[http://www.sbcl.org][SBCL]]. Other CL implementations
  may require slight changes.


* References

  - [[https://github.com/libre-man/unix-opts][unix-opts]] - Unix-style command line options parser
  - [[https://www.cliki.net/YAML][YAML]]: [[https://github.com/eudoxia0/cl-yaml][cl-yaml]] - A binding to the libyaml library
  - [[https://edicl.github.io/cl-ppcre/][cl-ppcre]] - Portable Perl-compatible regular expressions for Common Lisp

  Helping pages:

  - [[https://lispcookbook.github.io/cl-cookbook/os.html#running-external-programs][Running external programs]]
  - [[https://lispcookbook.github.io/cl-cookbook/scripting.html#parsing-command-line-arguments][Parsing command line arguments]]
  - [[http://index-of.es/Programming/Lisp/Lisp%20Mess/Erann%20Gat%20-%20Idiots%20Guide%20To%20Lisp%20Packages.pdf][The Complete Idiot’s Guide to Common Lisp Packages]]
  - [[https://www.reddit.com/r/Common_Lisp/comments/fbdshp/quick_shadowing_question_noob/][Quick shadowing question]] - reddit
  - [[https://www.cs.cmu.edu/Groups/AI/html/cltl/clm/node116.html][Name Conflicts]] - Common Lisp the Language, 2nd Edition
  - [[https://stackoverflow.com/questions/22000468/why-does-using-defpackage-result-in-a-name-conflict][Why does using defpackage result in a NAME-CONFLICT?]] - StackOverflow


* Usage

  Multicp has the following options:

  #+begin_example
    Multicp - a tool to copy files to a test system and execute commands there.

    Usage: multicp [-H|--help] [-p|--port ARG] [-h|--host ARG] [-u|--user ARG]
                   [-l|--list] [-d|--dryrun] [-v|--version] configuration

    Available options:
      -H, --help     print this help text
      -p, --port ARG port for the ssh connection
      -h, --host ARG target host name
      -u, --user ARG system user
      -l, --list     list all configurations
      -d, --dryrun   print only, don't execute
      -v, --version  show version information
  #+end_example

  Multicp requires a YAML file with the definitions of files, locations and commands to
  work. The configuration YAML file is divided into sections to separate configuration.
  The sections are:

  1. =system=: Defines hostname, port and user information of the target system.

  2. =settings=: Defines keywords that will be replaced with its values in paths, file names
     or commands.

  3. =init=: An init section that will be executed before any other section.

  4. =cleanup=: A clean-up section that will be executed after all other sections.

  5. File sections: Any number of 'work' sections, containing a source directory, a target
     directory, a list of files to copy and / or a list of commands to execute. These are
     executed after the =init= section and before the =cleanup= section.

  An example YAML config file can be found in the source code directory of the =multicp=
  project. It can be used as starting point.

  Multicp configuration YAML files are expected in the directory =$HOME/.config/multicp= and
  must have a =.yaml= extension.

  When specifying a configuration when =multicp= is executed, the file name of the
  configuration file must be used, but without it's =.yaml= extension. For example:
  =tool-test.yaml= is used as =tool-test=.


* License

  Copyright (C) 2006-2022  Peter Stoldt

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see https://www.gnu.org/licenses/.
