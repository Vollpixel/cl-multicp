################################################################
# Multi-CP Makefile
################################################################
#
# About Makefiles:
#
#   https://www.chemie.fu-berlin.de/chemnet/use/info/make/make_7.html
#   https://stackoverflow.com/questions/47888877/sbcl-building-a-standalone-executable
#
################################################################

TOOL_NAME = multicp
PROJECT_DIR = $(shell pwd)
TARGET_DIR = $(PROJECT_DIR)/build
TARGET = $(TARGET_DIR)/$(TOOL_NAME)

################################################################

all: clean build

build:
	mkdir $(TARGET_DIR)
	sbcl --load multicp.asd --eval '(ql:quickload :multicp)' --eval '(in-package :multicp)' --eval "(sb-ext:save-lisp-and-die #p\"$(TARGET)\" :toplevel #'main :executable t)"

clean:
	rm -rf $(TARGET_DIR)
